// var expect = require('chai').expect

describe('index.js: ', function() {
  it('isNum() should work fine.', function() {
    // expect(isNum).withArgs().to.throw(new Error('Arguments required.'))
    expect(isNum(1)).to.be.true
    expect(isNum('1')).to.be.false
  })
})